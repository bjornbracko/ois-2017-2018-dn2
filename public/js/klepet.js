// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.omemba=function(sporocilo) {
  var user;
  var besede=sporocilo.split(' ');
  var message="\u261E Omemba v klepetu";
  for (var i=0;i<besede.length;i++) {
    if(besede[i].indexOf('@')==0) {
      user=besede[i].split('@');
      if(user[1]!=trenutniVzdevek) {
        this.socket.emit('sporocilo', {vzdevek: user[1], besedilo: message});
      }
    } 
  }
};

Klepet.prototype.zOmemba=function(sporocilo) {
  var user;
  var b=sporocilo.split('"');
  var besede=b[3].split(' ');
  var message="\u261E Omemba v klepetu";
  for (var i=0;i<besede.length;i++) {
    if(besede[i].indexOf('@')==0) {
      user=besede[i].split('@');
      if(user[1]!=trenutniVzdevek) {
        this.socket.emit('sporocilo', {vzdevek: user[1], besedilo: message});
      }
    } 
  }
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
        if (parametri) {
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
        } else {
          sporocilo = 'Neznan ukaz';
        }
      break;
      
      case 'barva':
        besede.shift();
        var barva=besede.join(' ');
        document.querySelector("#sporocila").style.color=barva;
        document.querySelector("#kanal").style.color=barva;
        break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};